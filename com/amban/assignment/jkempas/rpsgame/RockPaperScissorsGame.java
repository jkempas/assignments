package com.amban.assignment.jkempas.rpsgame;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

public class RockPaperScissorsGame {
    static int roundNumber = 0;
    static int drawCount = 0;
    Player p1;
    Player p2;

    public static void main(String[] args) {
        char quit = 'c';
        
        RockPaperScissorsGame game = new RockPaperScissorsGame();   
        
        game.initialize();

        do {
            System.out.println("ROUND: " + (roundNumber + 1));

            game.play();

            System.out.println("Next round is up. Press q + enter to quit.");
            try {
                quit = game.getConsoleChar();
            } catch (IOException e) {
                System.out.println("Error at reading console input:" + e.getMessage());
            }

        } while(quit != 'q');
        
        game.showSummary();

    }

    public void initialize() {
        String name;
        System.out.println("ROCK PAPER AND SCISSORS GAME");
        System.out.println("----------------------------");
        System.out.println("Enter name for player 1:");
        
        p1 = new Player();
        p2 = new Player();

        name = System.console().readLine();
        if (name.isBlank()) name = "Player 1";
        p1.setName(name);

        System.out.println("Enter name for player 2:");

        name = System.console().readLine();
        if (name.isBlank()) name = "Player 2";
        p2.setName(name);
    }

    public void play() {
        int result = 0;
        getWeapon(p1);
        getWeapon(p2);
        
        roundNumber++;

        result = p1.fight(p2);

        System.out.println(p1.getName() + " uses " + p1.getCurrentWeaponAsString() + " as a weapon.");
        System.out.println(p2.getName() + " uses " + p2.getCurrentWeaponAsString() + " as a weapon.");

        if (result == 1) {
            System.out.println(p1.getName() + " wins this round.");
            p1.incScore();
        }
        else if (result == -1) {
            System.out.println(p2.getName() + " wins this round.");
            p2.incScore();
        }
        else
        {
            System.out.println("This round is drawn.");
            drawCount++;
        }
    }

    public char getConsoleChar() throws IOException {
        BufferedReader bufferReader =new BufferedReader(new InputStreamReader(System.in));
        return (char) bufferReader.read();
    }

    public void getWeapon(Player p) {
        //var weapon = System.console().
        char weaponChar = 'n';

        System.out.println("Enter weapon for " + p.getName() + ":"); 
        System.out.println("r) ROCK"); 
        System.out.println("p) PAPER "); 
        System.out.println("s) SCISSORS"); 

        try {
            while(weaponChar != 'r' && weaponChar != 'p' && weaponChar != 's') {               
                weaponChar = getConsoleChar();                
            }
        } catch (IOException e) {
            System.out.println("Error at reading console input:" + e.getMessage());
        }

        p.setCurrentWeapon(weaponChar);
    }

    public static void clearScreen() {  
        System.out.print("\033[H\033[2J");  
        System.out.flush();  
    }  
    public void showSummary() {
        var p1Score = p1.getScore();
        var p2Score = p2.getScore();

        System.out.println("Rounds played      : " + roundNumber);
        System.out.println("Rounds drawn       : " + drawCount);
        System.out.println("Rounds won by " + p1.getName() + " : " + p1Score);
        System.out.println("Rounds won by " + p2.getName() + " : " + p2Score);

        if (p1Score == p2Score) {
            System.out.println("The overall game is drawn.");    
        } else if (p1Score > p2Score) {
            System.out.println(p1.getName() + " wins the overall game.");    
        } else if (p1Score < p2Score) {
            System.out.println(p2.getName() + " wins the overall game.");    
        }
    }
}