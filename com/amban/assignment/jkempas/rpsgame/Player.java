package com.amban.assignment.jkempas.rpsgame;

enum Weapon {
  NONE,
  ROCK,
  PAPER,
  SCISSORS
}

public class Player {
    private String name = null;
    private int score = 0;
    private Weapon currentWeapon = Weapon.NONE;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int newScore) {
        this.score = newScore;
    }

    public void incScore()
    {
        this.score++;
    }

    public Weapon getCurrentWeapon() {
        return this.currentWeapon;
    }

    public void setCurrentWeapon(Weapon newWeapon) {
        this.currentWeapon = newWeapon;
    }

    public String getCurrentWeaponAsString() {
        switch(this.currentWeapon) {
            case ROCK:
                return "Rock";            
            case PAPER:
                return "Paper";
            case SCISSORS:
                return "Scissors";
            default:
                return "None";
        }
    }

    public void setCurrentWeapon(char c) {
        switch(c) {
            case 'r':
                this.currentWeapon = Weapon.ROCK;
                break;           
            case 'p':
                this.currentWeapon = Weapon.PAPER;
                break;   
            case 's':
                this.currentWeapon = Weapon.SCISSORS;
                break;
            default:
                this.currentWeapon = Weapon.NONE;
                break;
        }
    }

    /*
     * Returns: -1 if battle was lost by the player to player p2, 0 if the battle ended in a draw,
     * 1 if the player won.
     */
    public int fight(final Player p2) {
        //Player always wins a non existent opponent player2
        if (p2 == null) 
            return 1;

        if (this.currentWeapon == Weapon.NONE && p2.currentWeapon != Weapon.NONE)
            return -1;

        if (this.currentWeapon != Weapon.NONE && p2.currentWeapon == Weapon.NONE)
            return 1;

        //If both players have the same weapon the fight is drawn.
        if (this.currentWeapon == p2.currentWeapon)
            return 0;

        switch(currentWeapon) {
            case ROCK:
                if (p2.currentWeapon == Weapon.PAPER)
                    return -1;
                return 1;
            
            case PAPER:
                if (p2.currentWeapon == Weapon.SCISSORS)
                    return -1;
                return 1;

            case SCISSORS:
                if (p2.currentWeapon == Weapon.ROCK)
                    return -1;
                return 1;
            default:
                break;
        }

        //By default game is drawn and to make compiler happy as well.
        return 0;
    }
}
