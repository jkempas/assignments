# Rock Paper and Scissors Game Development Environment in Win 10

### Adopt Open JDK

- https://adoptopenjdk.net/?variant=openjdk14&jvmVariant=hotspot

- Download for Windows x64

- Add to PATH

- Set JAVA_HOME variable

- Default installation path:
  C:\Program Files\AdoptOpenJDK\jdk-14.0.2.12-hotspot\
  
### Visual Studio Code or some IDE for light weight development

  - If Visual Studio Code chosen as an editor: 
      * Install Java Extension Pack
       * Popular extensions for Java development and more
      
### Compile and Run Program
  In visual studio code terminal window or command line:
  
  - compile: 

    javac com\amban\assignment\jkempas\rpsgame\*.java

  - run: 

     java com\amban\assignment\jkempas\rpsgame\RockPaperScissorsGame.java

  - compile and run in Windows
     
     .\comrun.bat

#### NOTE: To export this document as PDF, install Markdown PDF extension for Visual Studio Code editor
           - Press F1 to reveal the export command "Markdown PDF: Export (pdf)" by typing "markdown"